/*
	Nome: Andr� Hallwas Ribeiro Alves 101528353
	Date: 13/04/17 15:37
	Description: Trabalho DosFipp
*/

#include"Bibilhotecas/Funcoes.h"
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<conio.h>

char _Selector(Manager *Cmd){
	return help(Cmd)? 1 :
			 nd(Cmd)? 1 :
	 leave(Cmd->key)? 1 :
	  		dir(Cmd)? 1 :
	  		cls(Cmd)? 1 :
	  		 cd(Cmd)? 1 :
	  		 md(Cmd)? 1 :
	  		 rd(Cmd)? 1 :
	  	   copy(Cmd)? 1 :
	   copy_Con(Cmd)? 1 :
	  		del(Cmd)? 1 :
	  	   type(Cmd)? 1 :
	  	   find(Cmd)? 1 :
	  		 fc(Cmd)? 1 :
       	   tree(Cmd)? 1 :
    		 f7(Cmd)? 1 :
	  mount(Cmd)? 1 : 0 ;
}

void _Loop(){

	Manager *Cmd = init_Cmd();

	strcpy(Cmd->P->Local,"\\Caminho");

	_Says(2,Cmd);/////Start Message;
	while(!leave(Cmd->key)){
		_Says(1,Cmd);
		_Capture(&Cmd);
		_Selector(Cmd)? printf("\n"):_Says(98,Cmd);
	}
}

int main(void){
	_Loop();
}
