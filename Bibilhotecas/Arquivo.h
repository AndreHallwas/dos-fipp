#ifndef ARQUIVO_H
#define ARQUIVO_H
#define TAM_LINHA 80
#include<dir.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#include<string.h>
#include"Estruturas.h"

Linha* criaLinha(Linha *Bottom, Linha *Top, Lista *Lin);
Lista* criaLista(Lista *ant, Lista *prox, char Info);
char vazia_Linha(Linha *lin);
char vazia_Lista(Lista *l);
void insereLista(Lista **l, char Info);
void inserirL(Linha **lin, char Info);
void insereLinha(Linha **lin, char Info);
char retiraLista(Lista **l);
void exibeLista(Lista *l);
void exibeLinha(Linha *lin, Lista *l);
char carregaArquivo(Manager **Cmd);
char abreArquivo(Manager *Cmd);
char apagaArquivo(Manager *Cmd);
char criaPasta(Manager *Cmd,ListaGen **l,ListaGen *Ppai,char *Nome);
char copiaArquivo(Manager *Cmd);

Linha* criaLinha(Linha *Bottom, Linha *Top, Lista *Lin) {
    /*
     *Cria Uma Linha;
     */
    Linha *aux = (Linha*) malloc(sizeof (Linha));
    aux->bottom = Bottom;
    aux->Linha = Lin;
    aux->LinhaF = Lin;
    aux->top = Top;
    aux->letras = 0;

    return aux;
}

Lista* criaLista(Lista *ant, Lista *prox, char Info) {
    /*
     *Cria Uma Lista;
     */
    Lista *aux = (Lista*) malloc(sizeof (Lista));
    aux->ant = ant;
    aux->prox = prox;
    aux->info = Info;

    return aux;
}

List* criaListaL(List *ant, List *prox, char Info[]) {
    /*
     *Cria Uma Lista;
     */
    List *aux = (List*) malloc(sizeof (List));
    aux->ant = ant;
    aux->prox = prox;
    strcpy(aux->info,Info);

    return aux;
}


char vazia_Linha(Linha *lin) {
    return lin == NULL;
}

char vazia_Lista(Lista *l) {
    return l == NULL;
}

void insereLista(Lista **l, char Info) {
    Lista *aux = *l;
    if (!vazia_Lista(aux)) {
        while (!vazia_Lista(aux->prox)) {
            aux = aux->prox;
        }
        aux->prox = criaLista(aux, null, Info);
    } else {
        *l = criaLista(null, null, Info);
    }
}

void inserirL(Linha **lin, char Info) {
    Lista *aux;
    if (!vazia_Linha(*lin)) {
        if (!vazia_Lista((*lin)->Linha)) {
            aux = criaLista((*lin)->LinhaF, NULL, Info); /////cria a letra e liga o final ao anterior;
            (*lin)->LinhaF->prox = aux; /////liga o proximo do final a letra;
            (*lin)->LinhaF = (*lin)->LinhaF->prox; /////atualiza o final;
        } else {
            aux = criaLista(NULL, NULL, Info); /////cria a letra liga a lista;
            (*lin)->LinhaF = (*lin)->Linha = aux; /////Caso menos possivel, a menos que haja erro;
        }
    } else {
        *lin = criaLinha(NULL, *lin, criaLista(NULL, NULL, Info)); /////cria uma linha com uma letra;
    }
    (*lin)->letras++;
}

void insereLinha(Linha **lin, char Info) {
    if (!vazia_Linha(*lin)) {
        if ((*lin)->letras < TAM_LINHA) {
            inserirL(&(*lin), Info); /////insere na linha;
        } else if (vazia_Linha((*lin)->bottom)) {
            inserirL(&(*lin)->bottom, Info); ///// insere na proxima linha caso alguma letra estore o limite da linha;
            (*lin)->bottom->top = *lin;
        } else {
            insereLinha(&(*lin)->bottom, Info);
        }
    } else {
        inserirL(&(*lin), Info); /////caso um ponteiro vazio seja enviado;
    }
}

void insereListaL(List **l, char Info[]) {
    List *aux = *l;
    if (aux!=null) {
        while (aux->prox!=null) {
            aux = aux->prox;
        }
        aux->prox = criaListaL(aux, null, Info);
    } else {
        *l = criaListaL(null, null, Info);
    }
}

void insere_Letras(ListaGen **l){
	char Aux;
	Aux = getche();
	while(Aux != 27){
		if(Aux == '\r'){
			printf("\n");
			Aux = '\n';
		}
		insereLinha(&(*l)->LArq,Aux);
		(*l)->Tamanho++;
		Aux = getche();
	}
}

void exibe_Letras(ListaGen *l){
	exibeLinha(l->LArq,l->LArq->Linha);
}

char retiraLista(Lista **l){
	Lista *aux = *l;
	char ret;
	if (!vazia_Lista(aux) ){
		*l = (*l)->prox;
		if(!vazia_Lista(*l))
        	(*l)->ant = NULL;
        ret = aux->info;
        free(aux);
        return ret;
    } 
    return -1;
}

void exibeLista(Lista *l) {
    if (!vazia_Lista(l)) {
        printf("%c ", l->info);
        exibeLista(l->prox);
    }
}

void exibeListaL(List *l) {
    if (l!=null) {
        printf("%s \n\n", l->info);
        exibeListaL(l->prox);
    }
}

List* exibeListaL_Ultimo(List *l) {
    if (l->prox!=null) {
        return exibeListaL_Ultimo(l->prox);
    }else{
    	return l;
    }
}

void exibeLinha(Linha *lin, Lista *l) {
    if (!vazia_Lista(l)) {
        printf("%c", l->info);
        exibeLinha(lin, l->prox);
    } else
    if (!vazia_Linha(lin->bottom)) {
        printf("\n");
        exibeLinha(lin->bottom, lin->bottom->Linha);
    }
}

char carregaArquivo(Manager **Cmd){
	FILE *arq = fopen((*Cmd)->Arq->Caminho,"r");
	char Aux;
	if(arq != null){
		if((*Cmd)->Arq == null){
			(*Cmd)->Arq = (Archive*)malloc(sizeof(Archive));
		}
		Aux = getc(arq);
		while(Aux != -1){
			insereLinha(&(*Cmd)->Arq->arquivo,Aux);
			Aux = getc(arq);
		}
		fclose(arq);		
		return 1;
	}
	fclose(arq);
	return 0;
}

char abreArquivo(Manager *Cmd){
	if(carregaArquivo(&Cmd)){
		exibeLinha(Cmd->Arq->arquivo,Cmd->Arq->arquivo->Linha);
		return 1;
	}
	return 0;
}

char apagaArquivo(Manager *Cmd){
	remove(Cmd->Arq->Caminho);
}

char criaPasta(Manager *Cmd,ListaGen **l,ListaGen *Ppai,char *Nome){
	/////mkdir(Cmd->Arq->Caminho);
}

char copiaArquivo(Manager *Cmd){
	if(carregaArquivo(&Cmd)){
		FILE *Novo = fopen(Cmd->Arq->NCaminho,"w+");
		if(Novo != null){
			Linha *aux = Cmd->Arq->arquivo;
			Lista *auxL;
			while(!vazia_Linha(aux)){
				auxL = aux->Linha;
				while(!vazia_Lista(auxL)){
					putc(auxL->info,Novo);
					auxL = auxL->prox;
				}
				aux = aux->bottom;
			}
			fclose(Novo);
			return 1;
		}	
	}
	return 0;
}

#endif

