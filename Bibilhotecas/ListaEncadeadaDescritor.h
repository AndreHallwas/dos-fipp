#ifndef LISTAENCADEADADESCRITOR_H
#define LISTAENCADEADADESCRITOR_H

#define TAM_LINHA 80
#define null NULL
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"Estruturas.h"

#define TAM_PALAVRA 400

typedef struct Palavras Palavra;

void initLista(Lista **l);
void initLinha(Linha **lin);
Linha* criaLinha(Linha *Bottom, Linha *Top, Lista *Lin);
Lista* criaLista(Lista *ant, Lista *prox, char Info);
Palavra* criaPalavra(Palavra *ant, Palavra *prox, char Info[]);
char vazia_Linha(Linha *lin);
char vazia_Lista(Lista *l);
char vazia_Palavra(Palavra *p);
char vazia_Descritor(descritor *d);
void inserirL(Linha **lin, char Info);
void insereLinha(Linha **lin, char Info);
void exibeLinha(Linha *lin, Lista *l);
void insereLista(Lista **l, char Info);
void inserePalavra(Palavra **p, char Info[]);
void insereLista(Lista **l, char *Info);
char retiraLista(Lista **l);
char* retiraPalavra(Palavra **p);
void exibeLista(Lista *l);

typedef struct Palavras{
	char *info;
    struct Palavras *ant;
    struct Palavras *prox;
}Palavra;

void initLista(Lista **l) {
    *l = NULL;
}

void initLinha(Linha **lin) {
    *lin = NULL;
}

Linha* criaLinha(Linha *Bottom, Linha *Top, Lista *Lin) {
    /*
     *Cria Uma Linha;
     */
    Linha *aux = (Linha*) malloc(sizeof (Linha));
    aux->bottom = Bottom;
    aux->Linha = Lin;
    aux->LinhaF = Lin;
    aux->top = Top;
    aux->letras = 0;

    return aux;
}

Lista* criaLista(Lista *ant, Lista *prox, char Info) {
    /*
     *Cria Uma Lista;
     */
    Lista *aux = (Lista*) malloc(sizeof (Lista));
    aux->ant = ant;
    aux->prox = prox;
    aux->info = Info;

    return aux;
}

Palavra* criaPalavra(Palavra *ant, Palavra *prox, char Info[]) {
    /*
     *Cria Uma Lista;
     */
   	Palavra *aux = (Palavra*) malloc(sizeof (Palavra));
   	aux->info = (char*)malloc(TAM_PALAVRA);
    aux->ant = ant;
    aux->prox = prox;
    strcpy(aux->info,Info);

    return aux;
}

char vazia_Linha(Linha *lin) {
    return lin == NULL;
}

char vazia_Lista(Lista *l) {
    return l == NULL;
}

char vazia_Palavra(Palavra *p) {
    return p == NULL;
}

char vazia_Descritor(descritor *d){
	return d == NULL;
}

void inserirL(Linha **lin, char Info) {
    Lista *aux;
    if (!vazia_Linha(*lin)) {
        if (!vazia_Lista((*lin)->Linha)) {
            aux = criaLista((*lin)->LinhaF, NULL, Info); /////cria a letra e liga o final ao anterior;
            (*lin)->LinhaF->prox = aux; /////liga o proximo do final a letra;
            (*lin)->LinhaF = (*lin)->LinhaF->prox; /////atualiza o final;
        } else {
            aux = criaLista(NULL, NULL, Info); /////cria a letra liga a lista;
            (*lin)->LinhaF = (*lin)->Linha = aux; /////Caso menos possivel, a menos que haja erro;
        }
    } else {
        *lin = criaLinha(NULL, *lin, criaLista(NULL, NULL, Info)); /////cria uma linha com uma letra;
    }
    (*lin)->letras++;
}

void insereLinha(Linha **lin, char Info) {
    if (!vazia_Linha(*lin)) {
        if ((*lin)->letras < TAM_LINHA) {
            inserirL(&(*lin), Info); /////insere na linha;
        } else if (vazia_Descritor((*lin)->bottom)) {
            inserirL(&(*lin)->bottom, Info); ///// insere na proxima linha caso alguma letra estore o limite da linha;
            (*lin)->bottom->top = *lin;
        } else {
            insereLinha(&(*lin)->bottom, Info);
        }
    } else {
        inserirL(&(*lin), Info); /////caso um ponteiro vazio seja enviado;
    }
}

void exibeLinha(Linha *lin, Lista *l) {
    if (!vazia_Lista(l)) {
        printf("%c", l->info);
        exibeLinha(lin, l->prox);
    } else
        if (!vazia_Descritor(lin->bottom)) {
        printf("\n");
        exibeLinha(lin->bottom, lin->bottom->Linha);
    }
}

void insereLista(Lista **l, char Info) {
    Lista *aux = *l;
    if (!vazia_Lista(aux)) {
        while (!vazia_Lista(aux->prox)) {
            aux = aux->prox;
        }
        aux->prox = criaLista(aux, null, Info);
    } else {
        *l = criaLista(null, null, Info);
    }
}

void inserePalavra(Palavra **p, char Info[]) {
    Palavra *aux = *p;
    if (!vazia_Palavra(aux)) {
        while (!vazia_Palavra(aux->prox)) {
            aux = aux->prox;
        }
        aux->prox = criaPalavra(aux, null, Info);
    } else {
        *p = criaPalavra(null, null, Info);
    }
}

void insereLista(Lista **l, char *Info) {
    for(int i =0 ; Info[i]!= '\0';i++){
    	insereLista(&(*l),Info[i]);
	}
}

char retiraLista(Lista **l){
	Lista *aux = *l;
	char ret;
	if (!vazia_Lista(aux) ){
		*l = (*l)->prox;
		if(!vazia_Lista(*l))
        	(*l)->ant = NULL;
        ret = aux->info;
        free(aux);
        return ret;
    } 
    return -1;
}

char* retiraPalavra(Palavra **p){
	Palavra *aux = *p;
	char *ret = (char*)malloc(200);
	if(!vazia_Palavra(*p)){	
		while(!vazia_Palavra(aux->prox)){
			aux = (aux)->prox;
	    } 
        (aux)->ant->prox = NULL;
        strcpy(ret,aux->info);
        free(aux);
        return ret;
	}       
    return null;
}

void exibeLista(Lista *l) {
    if (!vazia_Lista(l)) {
        printf("%c ", l->info);
        exibeLista(l->prox);
    }
}

#endif

