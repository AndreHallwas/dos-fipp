#ifndef UNIDADE_H
#define UNIDADE_H

#include <dir.h> // classe para manipular diret�rios
#include <dirent.h> // classe para manipular diret�rios
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <io.h>
#include "PilhaDinamica.h"
#include "Estruturas.h"
/////#include "String.h"

#include<ctype.h>

#define null NULL

void init_ListaD(ListaD **ld);
char vazia_ListaD(ListaD *ld);
ListaD* criaListaD(ListaGen *Head,ListaGen *Tail,ListaGen *Ppai,ListaGen *LArq,char Nome[],char Data[],char Hora[],char Tipo,int Tamanho);
void exibeListaD(ListaD *l, int i,char v[]);
void init_Unidade(Unidade **u);
char vazia_Unidade(Unidade *u);
Unidade* criaUnidade(Volume *Top,Volume *Bottom,char U,char A,ListaGen *IniDir,string *s);
ListaGen* busca_ListaGen(ListaGen *l,LString *s);
ListaGen* busca_Dir(Unidade *u,LString *s);
void Mount_Diretory(Manager **Cmd, ListaGen **l,ListaGen *Ppai);
Unidade* insereUnidade(Unidade **u,char Un);
void exibeUnidade(Unidade *u);
char retiraUnidade(Unidade **u);
char retiraUnidadeParam(Unidade **u,char param);
char buscaUnidade(Manager *Cmd,char U);
char monta(Manager **Cmd,char U);
void Mount_Tree(ListaD *l, int i,char v[]);
void Mount_Dir(ListaD *l);
char Posiciona(Manager **Cmd,char *p);
Manager* init_Cmd();

void init_ListaD(ListaD **ld){
	*ld = null;
}

char vazia_ListaD(ListaD *ld){
	return ld == null;
}

ListaD* criaListaD(ListaGen *Head,ListaGen *Tail,ListaGen *Ppai,ListaGen *LArq,char Nome[],char Data[],char Hora[],char Tipo,int Tamanho){
	ListaD *aux = (ListaD*)malloc(sizeof(ListaD));
	strcpy(aux->Data , Data);
	strcpy(aux->Hora , Hora);
	strcpy(aux->Nome , Nome);
	
    aux->Tipo = Tipo;
    aux->Tamanho = Tamanho;
	aux->Head = Head;
	aux->Tail = Tail;
	aux->Ppai = Ppai;
	aux->LArq = null;
	
	return aux;
}

void exibeListaD(ListaD *l, int i,char v[]){
	int j;
	if(!vazia_ListaD(l)){
		if(l->Tipo == '5'){
			for(j=0;j<=i;j++){
				printf("%c",v[j]);
			}
			printf("___%s\n",l->Nome);
			if(l->Tail!=null){
				v[i-2]='|';
			}
			if(l->Head!=null)
				v[i+2]='|';
		}
		
		exibeListaD(l->Head,i+2,v);
		/////v[i-2]=' ';
		/////v[i+2]=' ';
		exibeListaD(l->Tail,i,v);
	}
}

void init_Unidade(Unidade **u){
	*u = null;
}

char vazia_Unidade(Unidade *u){
	return u == null;
}

Unidade* criaUnidade(Volume *Top,Volume *Bottom,char U,char A,ListaGen *IniDir,string *s){
	Unidade *aux = (Unidade*)malloc(sizeof(Unidade));
	aux->Bottom = Bottom;
	aux->IniDir = IniDir;
	aux->Top = Top;
	aux->Unidade = toupper(U);
	aux->Apelido = toupper(A);
	aux->s = s;
	
	return aux;
}

ListaGen* busca_ListaGen(ListaGen *l,LString *s){
	if(!vazia_ListaD(l)){
		if(!stricmp(l->Nome,s->Info)){
			if(!vazia_LString(s->prox)){
				return busca_ListaGen(l->Head,s->prox);
			}
			return l->Head;
		}
		return busca_ListaGen(l->Tail,s);
	}
	return null;	
}

ListaGen* busca_Dir(Unidade *u,LString *s){
	if(!vazia_Unidade(u)){
		if(!stricmp(u->s->inicio->Info,s->Info)){
			if(!vazia_LString(s->prox)){
				return busca_ListaGen(u->IniDir,s->prox);
			}
			return u->IniDir;
		}
		return busca_Dir(u->Bottom,s);
	}
	return null;
}


ListaGen* busca_ListaGen1(ListaGen *l,LString *s){
	if(!vazia_ListaD(l)){
		if(!stricmp(l->Nome,s->Info)){
			if(!vazia_LString(s->prox)){
				return busca_ListaGen1(l->Head,s->prox);
			}
			return l;
		}
		return busca_ListaGen1(l->Tail,s);
	}
	return null;	
}

ListaGen* busca_Dir1(Unidade *u,LString *s){
	if(!vazia_Unidade(u)){
		if(!stricmp(u->s->inicio->Info,s->Info)){
			if(!vazia_LString(s->prox)){
				return busca_ListaGen1(u->IniDir,s->prox);
			}
			return u->IniDir;
		}
		return busca_Dir1(u->Bottom,s);
	}
	return null;
}


ListaGen* busca_ListaGen_CAnterior(ListaGen *l,LString *s,ListaGen **ant){
	if(!vazia_ListaD(l)){
		if(!stricmp(l->Nome,s->Info)){
			if(!vazia_LString(s->prox)){
				*ant = l->Head;
				return busca_ListaGen_CAnterior(l->Head,s->prox,&(*ant));
			}
			return l;
		}
		*ant = l;
		return busca_ListaGen_CAnterior(l->Tail,s,&(*ant));
	}
	return null;	
}

ListaGen* busca_Dir_CAnterior(Unidade *u,LString *s,ListaGen **ant){
	if(!vazia_Unidade(u)){
		if(!stricmp(u->s->inicio->Info,s->Info)){
			if(!vazia_LString(s->prox)){
				*ant = u->IniDir;
				return busca_ListaGen_CAnterior(u->IniDir,s->prox,&(*ant));
			}
			return u->IniDir;
		}
		return busca_Dir_CAnterior(u->Bottom,s,&(*ant));
	}
	return null;
}

ListaGen* insere_Pasta(ListaGen **l,ListaGen *Ppai,char *Nome){
	if(*l!=null){
		insere_Pasta(&(*l)->Tail,Ppai,Nome);
	}else{
		return *l = criaListaD(null,null,Ppai,null,Nome,__DATE__,__TIME__,'5',0);
	}
}

ListaGen* insere_ListaGen(ListaGen **l,LString *s,char *Nome){
	if(!vazia_ListaD(*l)){
		if(!stricmp((*l)->Nome,s->Info)){
			if(!vazia_LString(s->prox)){
				return insere_ListaGen(&(*l)->Head,s->prox,Nome);
			}
			return insere_Pasta(&(*l)->Head,*l,Nome);
		}
		return insere_ListaGen(&(*l)->Tail,s,Nome);
	}
	return null;	
}

ListaGen* insere_Dir(Unidade **u,LString *s,char *Nome){
	if(!vazia_Unidade(*u)){
		if(!stricmp((*u)->s->inicio->Info,s->Info)){
			if(!vazia_LString(s->prox)){
				return insere_ListaGen(&(*u)->IniDir,s->prox,Nome);
			}
			return insere_Pasta(&(*u)->IniDir,null,Nome);
		}
		return insere_Dir(&(*u)->Bottom,s,Nome);
	}
	return null;
}


void Mount_Diretory(Manager **Cmd, ListaGen **l,ListaGen *Ppai) {
	/*
	*Basicamente Monta a Lista Generalizada.
	*Insere na Lista Generalizada
	*Caso 1: Elementos do mesmo Diret�rio s�o Armazenados Na Cauda;
	*Caso 2: Um Elemento dentro do atual diret�rio � armazenado no head do mesmo, e os demais elementos do diret�rio seguem o Caso 1;
	*Obs: A Fun��o armazena na lista generalizada inicialmente o primeiro diret�rio e todos
	*os internos a ele, e depois volta armazenando todos os demais,ou seja vai at� o nivel mais
	*profundo do atual diret�rio e volta montando todos os demais.
	*/
	ListaGen *lAux,*Now;
    struct dirent *pent = NULL;
    DIR *pdir = NULL, *pdirteste = NULL;
    char *auxC = exibeCaminho((*Cmd)->USelected->s);
    char a[50], b[50];
    pdir = opendir(auxC);
    free(auxC);
    if (!(pdir == NULL)) {
	    while (pent = readdir(pdir))
	        // enquanto houver um diretorio ou arquivo para listar
	    {
	        if (!(pent == NULL)&&strcmp(pent->d_name,".")!=0&&strcmp(pent->d_name,"..")!=0)/////alterado !pent == null
	        {
		        ///////printf("%s\t%d\t", pent->d_name,pdir->dd_dta.attrib);
		        insereString(&(*Cmd)->USelected->s,pent->d_name);
		        auxC = exibeCaminho((*Cmd)->USelected->s);
		        pdirteste = opendir(auxC);
		        
		        
			    strftime(a, 20,"%d/%m/%Y",localtime(&pdir->dd_dta.time_create));
		    	strftime(b, 20,"%H:%M",localtime(&pdir->dd_dta.time_create));
		                
		        lAux = criaListaD(null,null,Ppai,null,pent->d_name,a,b,'0'/*pdir->dd_dta.attrib*/,pdir->dd_dta.size);
		        
	            if(!vazia_ListaD(*l)){
	        		Now->Tail = lAux;
	        		(Now)=(Now)->Tail;
				}else{
					(*l)=Now=lAux;
				}
				
		        // Testa se o subdiret�rio foi aberto
		        if (pdirteste == NULL) // n�o � um subdiret�rio
		        {
		        	Now->Tipo = '4';
		        } else {
		            Now->Tipo = '5';
		            closedir(pdirteste);
		            pdirteste = null;
		            Mount_Diretory(&(*Cmd),&Now->Head,Now);
		            // desalocando o ponteiro pois abriu a pasta
		        }
		        removeDaString(&(*Cmd)->USelected->s);
	        }
	    }
    }// Encerra o ponteiro para o diret�rio
    closedir(pdir);
}

void insere_ListaD(Manager **Cmd){
	/*Cmd.Arq.Caminho*/
}

Unidade* insereUnidade(Unidade **u,char Un){
	Unidade *aux;
	if(!vazia_Unidade(*u)){
		 aux = *u;
		while(!vazia_Unidade(aux->Bottom)){
			aux = aux->Bottom;
		}	
		return aux->Bottom = criaUnidade(aux,null,Un,'0',null,null);
	}else{
		return *u = criaUnidade(null,null,Un,'0',null,null);
	}
}

void exibeUnidade(Unidade *u){
	if(!vazia_Unidade(u)){
		printf("%c ",u->Unidade);
		exibeUnidade(u->Bottom);
	}	
}

char retiraUnidade(Unidade **u){
	Unidade *aux = *u;
	char ret;
	if(!vazia_Unidade(*u)){	
		if(!vazia_Unidade((aux->Bottom))){
			while(!vazia_Unidade(aux->Bottom)){
				aux = (aux)->Bottom;
	    	} 
	    	aux->Top->Bottom = null;
		}else{
			*u = null;
		}
		ret = aux->Unidade;
        free(aux);
        return ret;
	}       
    return -1;
}

char retiraUnidadeParam(Unidade **u,char param){
	Unidade *aux = *u;
	char ret;
	if(!vazia_Unidade(*u)){	
		while(!vazia_Unidade(aux->Bottom)&&aux->Unidade!=toupper(param)){
			aux = (aux)->Bottom;
    	} 
    	if(aux->Unidade==toupper(param)){
    		if(!vazia_Unidade(aux->Top)){
    			aux->Top->Bottom = null;
			}else{
				*u = null;
			}
			ret = aux->Unidade;
	        free(aux);
	        return ret;
		}	
	}       
    return -1;
}

char buscaUnidade(Manager *Cmd,char U){
	Unidade *aux = Cmd->Unidades;
	char Count = 0;
	while(!vazia_Unidade(aux)&&aux->Unidade!=toupper(U)){
		aux = aux->Bottom;
		Count++;
	}
	if(!vazia_Unidade(aux)&&aux->Unidade==toupper(U)){
		return Count;
	}
	return -1;
}

char monta(Manager **Cmd,char U){
	char Pos, i ;
	char auxSt[3];
	if(*Cmd != NULL){
		Pos = buscaUnidade((*Cmd),U);
		auxSt[1]=':';
		auxSt[2]='\0';
		if(Pos==-1){
			(*Cmd)->USelected = insereUnidade(&(*Cmd)->Unidades,U);
			
		    auxSt[0]=(*Cmd)->USelected->Unidade;
		    insereString(&(*Cmd)->USelected->s,auxSt);
		    
			/////Mount_Diretory(&(*Cmd),&(*Cmd)->USelected->IniDir,null);
			if((*Cmd)->P->Local==null){
				(*Cmd)->P->Local = (char*)malloc(TAM_PATH);
			}
			strcpy((*Cmd)->P->Local,auxSt);
		}else{
			Unidade *Aux = (*Cmd)->Unidades;
			for(i = 0; i < Pos; i++){
				Aux = Aux->Bottom;
			}
			(*Cmd)->USelected = Aux;
			auxSt[0] = (*Cmd)->USelected->Unidade;
			strcpy((*Cmd)->P->Local,auxSt);
		}
		return 1;
	}
	return 0;
}


void Mount_Tree(ListaD *l, int i,char v[]){	
	int j;	
	if(!vazia_ListaD(l)){
		if(l->Tipo == '5'){
			for(j=0;j<=i;j++){
				printf("%c",v[j]);
			}
			printf("___%s\n",l->Nome);
			if(l->Tail!=null){
				v[i-2]='|';
			}
			if(l->Head!=null)
				v[i+2]='|';
		}
		
		Mount_Tree(l->Head,i+2,v);
		/////v[i-2]=' ';
		/////v[i+2]=' ';
		Mount_Tree(l->Tail,i,v);
	}		
}

void remove_Diretorio(ListaGen *l,ListaGen **lAnt){
	if(l != *lAnt){
		if(l->Tail == null){
			(*lAnt)->Tail = null;
		}else{
			(*lAnt)->Tail = l->Tail;
		}
	}else{
		if((*lAnt)->Ppai!=null){
			if((*lAnt)->Tail == null){
				(*lAnt)->Ppai->Head = null;
			}else{
				(*lAnt)->Ppai->Head = (*lAnt)->Tail;
			}
		}
	}		
}
void copia_Caixa(ListaGen *l,ListaGen **novo,ListaGen *Ppai){
	*novo = criaListaD(null,null,Ppai,l->LArq,l->Nome,l->Data,l->Hora,l->Tipo,l->Tamanho);	        
}

void copia_Arquivo(ListaGen *l,ListaGen **novo,ListaGen *Ppai){
	if(l!=null){
		copia_Caixa(l,&(*novo),Ppai);
		copia_Arquivo(l->Tail,&(*novo)->Tail,Ppai);
		copia_Arquivo(l->Head,&(*novo)->Head,*novo);
	}
}

void remove_Arquivo(ListaGen *l,ListaGen **lAnt){
	if(l != *lAnt){
		if(l->Tail == null){
			(*lAnt)->Tail = null;
		}else{
			(*lAnt)->Tail = l->Tail;
		}
	}else{
		if((*lAnt)->Ppai!=null){
			if((*lAnt)->Tail == null){
				(*lAnt)->Ppai->Head = null;
			}else{
				(*lAnt)->Ppai->Head = (*lAnt)->Tail;
			}
		}
	}		
}

void Mount_Dir(ListaD *l){	
	char Aux[50];
	char *buffer;
	char *auxI;
	int i;
	Pilha *auxP = null,*Nova = null;
	if(!vazia_ListaD(l)){
		
		buffer = (char*)malloc(50);
		auxI = (char*)malloc(1);
		i = 0;
		sprintf(Aux,"%d",l->Tamanho);
		
		while(Aux[i] != '\0'){
			pilha_push(&auxP,Aux[i]);
			i++;
		}
		i = 0;
		while(auxP != null){
			if(i>2){
				pilha_push(&Nova,'.');
				i = 0;
			}
			i++;
			pilha_pop(&auxP,auxI);
			pilha_push(&Nova,*auxI);
		}
		i = 0;
		while(Nova != null){
			pilha_pop(&Nova,auxI);
			buffer[i++] = *auxI;
		}
		buffer[i] = '\0';
		
		if(l->Tipo!='5'){
			printf("%15s\t%6s\t<FILE>\t%15s\t%.100s\n",l->Data,l->Hora,buffer,l->Nome);
		}else{
			printf("%15s\t%6s\t<DIR>\t%15s\t%.100s\n",l->Data,l->Hora," ",l->Nome);
		}
		free(buffer);
		free(auxI);
		Mount_Dir(l->Tail);
	}		
}

char Posiciona(Manager **Cmd,char *p){
	strcpy((*Cmd)->P->Local,p);
}

Manager* init_Cmd(){
	/*
	*Inicia A Estrutura de controle;
	*/
	Manager *Cmd = (Manager*)malloc(sizeof(Manager));
	Cmd->key = (KEY*)malloc(sizeof(KEY));
	Cmd->P = (Patch*)malloc(sizeof(Patch));
	Cmd->P->Local = (char*)malloc(TAM_PATH);
	Cmd->Unidades = null;
	Cmd->key->Com = null;
	Cmd->Arq = (Archive*)malloc(sizeof(Archive));
	Cmd->Arq->arquivo = null;
	Cmd->Commands = null;
	return Cmd;
}

#endif

