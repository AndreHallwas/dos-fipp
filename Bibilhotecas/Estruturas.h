#ifndef ESTRUTURAS_H
#define ESTRUTURAS_H
#define ListaGen ListaD
#define Volume Unidade
#include"String.h"

typedef struct patch Patch;
typedef struct Command Manager;
typedef struct patch Patch;
typedef struct KEY KEY;
typedef struct Volume Unidade;
typedef struct Arquivo Archive;
typedef struct descritor Linha;
typedef struct listaE Lista;
typedef struct listaL List;
typedef struct ListaGen ListaD;


typedef struct ListaGen{
	char Nome[200];
	char Data[20];
	char Hora[120];
    char Tipo;
    int Tamanho;
    struct ListaGen *Head;
	struct ListaGen *Tail;
	struct ListaGen *Ppai;
	Linha *LArq;
}ListaD;

typedef struct listaE {
    char info;
    struct listaE *ant;
    struct listaE *prox;
} Lista;

typedef struct listaL {
    char info[200];
    struct listaL *ant;
    struct listaL *prox;
} List;

typedef struct descritor {
    char letras; /////Quantidade
    Lista *Linha; 
    Lista *LinhaF;
    struct descritor *bottom;
    struct descritor *top;
} Linha;

typedef struct Arquivo{
	Linha *arquivo;
	char *Caminho;	
	char *NCaminho;
}Archive;

typedef struct Volume{
	struct Volume *Top;
	struct Volume *Bottom;
	char Unidade;
	char Apelido;
	string *s;
	ListaD *IniDir;
}Unidade;

typedef struct KEY{
	char active;
	char Letters[50];
	string *Com;
}KEY;

typedef struct patch{
	char *Local;
}Patch;

typedef struct Command{
	KEY *key;
	Patch *P;
	Archive *Arq;
	Unidade *Unidades;
	Unidade *USelected;
	List *Commands;
}Manager;

#endif
