#ifndef FUNCOES_H
#define FUNCOES_H
#include "PilhaDinamica.h"
#include "Estruturas.h"
#include "Unidade.h"
#include "Arquivo.h"

void _Capture(Manager **Cmd);
string* MountDirectory_Path(char *aux);
char MountDirectory_Path_Common(Manager **Cmd);
char _Says(const char param,Manager *Cmd);
char leave(KEY *Key);
char help(Manager *Cmd);
char dir(Manager *Cmd);
char cls(Manager *Cmd);
char cd(Manager *Cmd);
char md(Manager *Cmd);
char rd(Manager *Cmd);
char copy(Manager *Cmd);
char del(Manager *Cmd);
char type(Manager *Cmd);
char find(Manager *Cmd);
char fc(Manager *Cmd);
char nd(Manager *Cmd);
char tree(Manager *Cmd);
char mount(Manager *Cmd);
char f7(Manager *Cmd);

void _Capture(Manager **Cmd){
	char aux[400];
	char *Com;
	gets(aux);
	insereListaL(&(*Cmd)->Commands,aux);
	Com = strtok(aux," ");
	if(!vazia_String((*Cmd)->key->Com)){
		free((*Cmd)->key->Com);
		(*Cmd)->key->Com = null;
	}
	if(Com!=null){
		strcpy((*Cmd)->key->Letters,Com);
		Com = strtok(null," -");
		while(Com!=null){
			insereString(&(*Cmd)->key->Com,Com);
			Com = strtok(null," ");
		}
	}else{
		strcpy((*Cmd)->key->Letters,"\0");
	}
}


char* _Broke(Manager *Cmd){
	char aux[400];
	char auxE[400];
	char *Com;
	List *auxL = exibeListaL_Ultimo((Cmd)->Commands);
	strcpy(aux,auxL->info);
	Com = strtok(aux," ");
	if(Com!=null){
		strcpy(auxE,Com);
		Com = strtok(null," -");
		Com = strtok(null," ");
	}
	return Com;
}


char* _Broke_2Param(Manager *Cmd){
	char aux[400];
	char auxE[400];
	char *Com;
	List *auxL = exibeListaL_Ultimo((Cmd)->Commands);
	strcpy(aux,auxL->info);
	Com = strtok(aux," ");
	if(Com!=null){
		strcpy(auxE,Com);
		Com = strtok(null," -");
	}
	return Com;
}

string* MountDirectory_Path(char *param){
	string *s = null;
	char aux[400];
	strcpy(aux,param);/*a retirar*/
	char *Com;
	Com = strtok(aux,"\\");
	if(Com!=null){
		insereString(&s,Com);
		Com = strtok(null,"\\");
		while(Com!=null){
			insereString(&s,Com);
			Com = strtok(null,"\\");
		}
		return s;
	}
	return null;
}

char MountDirectory_Path_Common(Manager **Cmd){
	LString *aux;
	if(!vazia_String((*Cmd)->key->Com)){
		free((*Cmd)->Arq->Caminho);
		(*Cmd)->Arq->Caminho = (char*)malloc(400);
		aux = (*Cmd)->key->Com->inicio;
		if(!vazia_LString(aux)){
			strcpy((*Cmd)->Arq->Caminho,aux->Info);
			aux = aux->prox;
			while(!vazia_LString(aux)){
				strcat((*Cmd)->Arq->Caminho," ");
				strcat((*Cmd)->Arq->Caminho,aux->Info);
				aux = aux->prox;
			}
		}
		free((*Cmd)->key->Com);
		(*Cmd)->key->Com = null;
		return 1;
	}
	return 0;
}

char _Says(const char param,Manager *Cmd){
	char i;
	switch(param){
		case 1:
			printf("%s> ",Cmd->P->Local);
		break;
		case 2:
			printf("DOS Fipp [Versao 0.0.2.8]");
			printf("\n2017 Unoeste - Ciencia da Computacao\n\n");
		break;
		case 58:
			printf("\n");
			for(i = 59;i<71;i++)
				_Says(i,Cmd);
			printf("\n");
		break;
		case 12:
			printf("\nN�o foi possivel abrir o Caminho\n");
		break;
		case 59:
			printf("%.20s\t\t%.100s\n","Dir","Exibe uma lista de arquivos e\n\t\tsubdiretorios em um diretorio.");
		break;
		case 60:
			printf("%.20s\t\t%.100s\n","Cls","Limpa a tela.");
		break;
		case 61:
			printf("%.20s\t\t%.100s\n","CD","Exibe o nome do diretorio atual ou faz\n\t\talteracoes nele.");
		break;
		case 62:
			printf("%.20s\t\t%.100s\n","MD","Cria um diretorio.");
		break;
		case 63:
			printf("%.20s\t\t%.100s\n","RD","Remove um diretorio.");
		break;
		case 64:
			printf("%.20s\t\t%.100s\n","Copy","Copia um ou mais arquivos para outro local.");
		break;
		case 65:
			printf("%.20s\t%.100s\n","Copy Con");
		break;
		case 66:
			printf("%.20s\t\t%.100s\n","Del","Exclui um ou mais arquivos.");
		break;
		case 67:
			printf("%.20s\t\t%.100s\n","Type","Exibe o conte�do de um arquivo de texto.");
		break;
		case 68:
			printf("%.20s\t\t%.100s\n","Find","Procura uma cadeia de caracteres de texto em um\n\t\tou mais arquivos.");
		break;
		case 69:
			printf("%.20s\t\t%.100s\n","FC","Compara dois arquivos ou grupos de arquivos e exibe\n\t\tas diferen�as entre eles.");
		break;
		case 70:
			printf("%.20s\t\t%.100s\n","Tree","Exibe graficamente a estrutura de diretorios de\n\t\tuma unidade ou caminho.");
		break;
		case 98:
			printf("'%s' nao e reconhecido como um comando interno\nou externo, um programa operavel ou um arquivo em lotes.\n\n",Cmd->key->Letters);
		break;
	}
	return 1;
}

char leave(KEY *Key){/////Feito
	return !stricmp(Key->Letters,"exit");
}

char help(Manager *Cmd){/////Feito
	return (!stricmp(Cmd->key->Letters,"help")) ? _Says(58,Cmd) : 0;
}

char dir(Manager *Cmd){
	string *aux;
	if(!stricmp(Cmd->key->Letters,"dir")){
		if(MountDirectory_Path_Common(&Cmd)){/////POssivel Erro FUtuRO
			aux = MountDirectory_Path(Cmd->Arq->Caminho);
			Mount_Dir(busca_Dir(Cmd->Unidades,aux->inicio));
			return 1;
		}else if(Cmd->P!=null){
			aux = MountDirectory_Path(Cmd->P->Local);
			if(!vazia_String(aux)){
				Mount_Dir(busca_Dir(Cmd->Unidades,aux->inicio));
				return 1;
			}
		}
		return 12;
	}
	return 0;
}

char cls(Manager *Cmd){/////Feito
	return (!stricmp(Cmd->key->Letters,"cls")) ? !system("cls") : 0;
}

char cd(Manager *Cmd){
	string *aux;
	char *auxN;
	char auxTemp[40];
	if(!stricmp(Cmd->key->Letters,"cd")){
		if(MountDirectory_Path_Common(&Cmd)){
			aux = MountDirectory_Path(Cmd->Arq->Caminho);
			auxN = aux->Fim->Info;
			strcpy(auxTemp,auxN);
			if(!stricmp(auxN,aux->inicio->Info )&& auxTemp[2]!='.'){
				aux = MountDirectory_Path(Cmd->P->Local);
				insereString(&aux,auxN);
			}
			if(auxTemp[2]=='.'){
				auxN[2]='\0';
			}
			if(busca_Dir1(Cmd->Unidades,aux->inicio) != null){
				Posiciona(&Cmd,exibeCaminho(aux));
			}
			return 1;
		}
		return 12;
	}
	return 0;
}

char md(Manager *Cmd){
	string *aux;
	char *auxN;
	if(!stricmp(Cmd->key->Letters,"md")){
		if(MountDirectory_Path_Common(&Cmd)){
			aux = MountDirectory_Path(Cmd->Arq->Caminho);
			auxN = aux->Fim->Info;
			if(!stricmp(auxN,aux->inicio->Info)){
				aux = MountDirectory_Path(Cmd->P->Local);
				insereString(&aux,auxN);
			}
			if(busca_Dir1(Cmd->Unidades,aux->inicio)==null){
				removeDaString(&aux);
				insere_Dir(&(Cmd->Unidades),aux->inicio,auxN);
			}
			return 1;
		}
		return 12;
	}
	return 0;

}

char rd(Manager *Cmd){
	ListaGen *ant;
	ListaGen *now;
	string *aux;
	char *auxN;
	if(!stricmp(Cmd->key->Letters,"rd")){
		if(MountDirectory_Path_Common(&Cmd)){
			aux = MountDirectory_Path(Cmd->Arq->Caminho);
			auxN = aux->Fim->Info;
			if(!stricmp(auxN,aux->inicio->Info)){
				aux = MountDirectory_Path(Cmd->P->Local);
				insereString(&aux,auxN);
			}

			now = busca_Dir_CAnterior(Cmd->Unidades,aux->inicio,&ant);
			if(now!=null&&now->Tipo == '5'){
				remove_Diretorio(now,&ant);
			}
			return 1;
		}
		return 12;
	}
	return 0;
}

char copy(Manager *Cmd){
	ListaGen *ant;
	ListaGen *now;
	string *aux;
	char *aux1;
	char *auxN;
	char value[30];
	if(!stricmp(Cmd->key->Letters,"copy")){
		if(MountDirectory_Path_Common(&Cmd)){
			aux = MountDirectory_Path(Cmd->Arq->Caminho);
			auxN = _Broke_2Param(Cmd);
			/*
			scanf("%s",&value);*/
			aux1 = _Broke(Cmd);

			if(!stricmp(auxN,aux->inicio->Info)){
				aux = MountDirectory_Path(Cmd->P->Local);
				insereString(&aux,auxN);
			}
			now = busca_Dir_CAnterior(Cmd->Unidades,aux->inicio,&ant);
			if(now!=null&&now->Tipo == '4'){

			}
			return 1;
		}
		return 12;
	}
	return 0;
}

char copy_Con(Manager *Cmd){
	ListaGen *l;
	string *aux;
	char *auxN;
	if(!stricmp(Cmd->key->Letters,"copycon")){
		if(MountDirectory_Path_Common(&Cmd)){
			aux = MountDirectory_Path(Cmd->Arq->Caminho);
			auxN = aux->Fim->Info;
			if(!stricmp(auxN,aux->inicio->Info)){
				aux = MountDirectory_Path(Cmd->P->Local);
				insereString(&aux,auxN);
			}
			if(busca_Dir1(Cmd->Unidades,aux->inicio)==null){
				removeDaString(&aux);
				l = insere_Dir(&(Cmd->Unidades),aux->inicio,auxN);
				l->Tipo = '4';
			}
			printf("\n");
			insere_Letras(&l);
			printf("\n\n");
			return 1;
		}
		return 12;
	}
	return 0;
}

char del(Manager *Cmd){
	ListaGen *ant;
	ListaGen *now;
	string *aux;
	char *auxN;
	if(!stricmp(Cmd->key->Letters,"del")){
		if(MountDirectory_Path_Common(&Cmd)){
			aux = MountDirectory_Path(Cmd->Arq->Caminho);
			auxN = aux->Fim->Info;
			if(!stricmp(auxN,aux->inicio->Info)){
				aux = MountDirectory_Path(Cmd->P->Local);
				insereString(&aux,auxN);
			}

			now = busca_Dir_CAnterior(Cmd->Unidades,aux->inicio,&ant);
			if(now!=null&&now->Tipo == '4'){
				remove_Arquivo(now,&ant);
			}
			return 1;
		}
		return 12;
	}
	return 0;
}

char type(Manager *Cmd){
	ListaGen *l;
	string *aux;
	char *auxN;
	if(!stricmp(Cmd->key->Letters,"type")){
		if(MountDirectory_Path_Common(&Cmd)){
			aux = MountDirectory_Path(Cmd->Arq->Caminho);
			auxN = aux->Fim->Info;
			if(!stricmp(auxN,aux->inicio->Info)){
				aux = MountDirectory_Path(Cmd->P->Local);
				insereString(&aux,auxN);
			}
			l = busca_Dir1(Cmd->Unidades,aux->inicio);
			if(l != null){
				printf("\n\n");
				exibe_Letras(l);
				printf("\n\n");
			}
			return 1;
		}
		return 12;
	}
	return 0;
}

char find(Manager *Cmd){
	return !stricmp(Cmd->key->Letters,"find");
}

char fc(Manager *Cmd){
	return !stricmp(Cmd->key->Letters,"fc");
}

char nd(Manager *Cmd){
	return !stricmp(Cmd->key->Letters,"\0");
}

char tree(Manager *Cmd){
	int i;
	char v[80] = {' '};
	if(!stricmp(Cmd->key->Letters,"tree")){
		i = 1;
	    if(Cmd->USelected!=null){
	    	Mount_Tree(Cmd->USelected->IniDir,i,v);
		}
		return 1;
	}
	return 0;
}

char mount(Manager *Cmd){
	if(!stricmp(Cmd->key->Letters,"mount")){
		monta(&Cmd,Cmd->key->Com->inicio->Info[0]);
		removeDaString(&Cmd->key->Com);
		return 1;
	}
	return 0;
}

char f7(Manager *Cmd){
	if(!stricmp(Cmd->key->Letters,"f7")){
		exibeListaL(Cmd->Commands);
		return 1;
	}
	return 0;
}


#endif // FUNCOES_H
