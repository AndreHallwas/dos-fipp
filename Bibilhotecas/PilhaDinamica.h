#ifndef PILHADINAMICA_H
#define PILHADINAMICA_H
#include <stdio.h>
#include<string.h>
#include<stdlib.h>
#include<conio.h>

typedef struct pilha Pilha;

////////Pilha////////////
typedef struct pilha{
	char info;
	struct pilha *prox;
}Pilha;
/////////////////////////

/*
*Definido em PilhaDinamica.h
*/
void pilha_Init(Pilha **Palavra);
char pilha_isEmpty(Pilha *Palavra);
char pilha_top(Pilha *Palavra);
void pilha_pop(Pilha **Palavra,char *Letra);
void pilha_push(Pilha **Palavra,char Letra);
void pilha_listar(Pilha **Palavra);
void pilha_listarSemRemover(Pilha **Palavra);
void pilha_gravarSemRemover(Pilha **Palavra ,FILE *arq);
Pilha* criaPalavra(char Palavra[]);
Pilha* pilha_invertePalavra(Pilha *Palavra);
Pilha* pilha_copiarPalavra(Pilha **Palavra);

void pilha_Init(Pilha **Palavra){///Inicializa a pilha dinamica na posi��o nula;
	*Palavra = NULL;
}

char pilha_isEmpty(Pilha *Palavra){////Verifica se a pilha est� vazia;
	return Palavra == NULL;
}

char pilha_top(Pilha *Palavra){//////////Acessa o elemento no topo da pilha;
	if(!pilha_isEmpty(Palavra)){
		return Palavra->info;
	}
	return -1;
}

void pilha_pop(Pilha **Palavra,char *Letra){/////Remove um elemento do topo da pilha;
	Pilha *aux;
	if(!pilha_isEmpty(*Palavra)){
		aux = *Palavra;
		Letra[0] = (*Palavra)->info;
		*Palavra = aux->prox;
		free(aux);
	}else{
		*Letra = -1;
	}
	/////return Letra;
}

void pilha_push(Pilha **Palavra,char Letra){//////Insere um elemento no topo da pilha;
	Pilha *aux = (Pilha*)malloc(sizeof(Pilha));
	aux->info = Letra;
	aux->prox = *Palavra;
	*Palavra = aux;
}

void pilha_listar(Pilha **Palavra){////////////Lista todos os elementos da pilha;
	char *Letra = (char*)malloc(1);
	while(!pilha_isEmpty(*Palavra)){
		pilha_pop(&(*Palavra),Letra);
		printf("%s",Letra);
	}
}

void pilha_listarSemRemover(Pilha **Palavra){
	char *Letra = (char*)malloc(1);
	Pilha *auxPilha;
	pilha_Init(&auxPilha);
	while(!pilha_isEmpty(*Palavra)){
		pilha_pop(&(*Palavra) ,Letra);
		pilha_push(&auxPilha ,*Letra);
		printf("%s",Letra);
	}
	while(!pilha_isEmpty(auxPilha)){
		pilha_pop(&auxPilha ,Letra);
		pilha_push(&(*Palavra) ,*Letra);
	}
}

void pilha_gravarSemRemover(Pilha **Palavra ,FILE *arq){
	char *Letra = (char*)malloc(1);
	Pilha *auxPilha;
	pilha_Init(&auxPilha);
	while(!pilha_isEmpty(*Palavra)){
		pilha_pop(&(*Palavra) ,Letra);
		pilha_push(&auxPilha ,*Letra);
	}
	while(!pilha_isEmpty(auxPilha)){
		pilha_pop(&auxPilha ,Letra);
		pilha_push(&(*Palavra) ,*Letra);
		putc((int)Letra,arq);
	}
}

Pilha* criaPalavra(char Palavra[]){
    Pilha *NovaPilha ;
    int i = 0;
    /////strcat(Palavra,"\n");
    pilha_Init(&NovaPilha);
    do{
        i++;
    }while(Palavra[i] != '\0');
    i--;
    while(i>=0){
        pilha_push(&NovaPilha,Palavra[i--]);
    }

    return NovaPilha;
}

Pilha* pilha_invertePalavra(Pilha *Palavra){
    Pilha *NovaPilha;
    char *Letra = (char*)malloc(1);
    pilha_Init(&NovaPilha);
    while(!pilha_isEmpty(Palavra)){
        pilha_pop(&Palavra,Letra);
		pilha_push(&NovaPilha,*Letra);
	}
    return NovaPilha;
}

Pilha* pilha_copiarPalavra(Pilha **Palavra){
	char *Letra = (char*)malloc(1);
	Pilha *auxPilha ,*copia;
	pilha_Init(&auxPilha);
	pilha_Init(&copia);
	while(!pilha_isEmpty(*Palavra)){
		pilha_pop(&(*Palavra),Letra);
		pilha_push(&auxPilha ,*Letra);
	}
	while(!pilha_isEmpty(auxPilha)){
		pilha_pop(&auxPilha,Letra);
		pilha_push(&(*Palavra) ,*Letra);
		pilha_push(&copia ,*Letra);
	}
	return copia;
}
#endif
