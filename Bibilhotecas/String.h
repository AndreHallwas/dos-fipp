#ifndef STRING_H
#define STRING_H

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define null NULL
#define TAM_PATH 400

#define string String

typedef struct Lstring LString;
typedef struct String string;

char vazia_LString(LString *ls);
char vazia_String(String *s);
LString* criaLString(LString *ant,LString *prox,char *Info);
string* criaString(LString *inicio);
void insereString(string **s,char palavra[]);
char* retiraString(string **s);
void removeDaString(string **s);
void exibeString(string *s);
char* exibeCaminho(string *s);

typedef struct Lstring{
	char *Info;
	struct Lstring *ant,*prox;
}LString;

typedef struct String{
	LString *inicio,*Fim;
}string;

char vazia_LString(LString *ls){
	return ls == null;
}

char vazia_String(string *s){
	return s == null;
}

LString* criaLString(LString *ant,LString *prox,char *Info){
	LString *s = (LString*)malloc(sizeof(LString));
	
	char *aux = (char*)malloc(TAM_PATH);/////Simplificar
	strcpy(aux,Info);
	
	s->ant = ant;
	s->prox = prox;
	s->Info = aux;
	
	return s;
}

string* criaString(LString *inicio){
	string *s = (string*)malloc(sizeof(string));
	
	s->inicio = s->Fim = inicio;
	
	return s;
}

void insereString(string **s,char palavra[]){
	if(!vazia_String(*s)){
		if(!vazia_LString((*s)->Fim)){
			LString *aux = criaLString((*s)->Fim,null,palavra);
			(*s)->Fim->prox = aux;
			(*s)->Fim = aux;
		}else{
			(*s)->Fim = (*s)->inicio = criaLString(null,null,palavra);
		}
	}else{
		(*s)=criaString(criaLString(null,null,palavra));
	}
}

char* retiraString(string **s){
	if(!vazia_String(*s))
		if(!vazia_LString((*s)->inicio)){
			LString *aux = (*s)->inicio;
			char *auxC = (char*)malloc(TAM_PATH);
			strcpy(auxC,(*s)->inicio->Info);
			if(!vazia_LString(aux->prox)){
				(*s)->inicio = (*s)->inicio->prox;
				(*s)->inicio->ant = null;
			}else{
				(*s)->inicio = null;
			}
			free(aux);
			return auxC;
		}
	return null;
}

void removeDaString(string **s){
	if(!vazia_String(*s))
		if(!vazia_LString((*s)->Fim)){
			LString *aux = (*s)->Fim;
			if(!vazia_LString(aux->ant)){
				(*s)->Fim = (*s)->Fim->ant;
				(*s)->Fim->prox = null;
			}else{
				(*s)->Fim = null;
			}
			free(aux);
		}
}

void exibeString(string *s){
	LString *l = (s)->inicio;
	while(!vazia_LString(l)){
		printf("%s ",l->Info);
		l = l->prox;
	}
}

char* exibeCaminho(string *s){
	LString *aux = s->inicio;
	char *auxS = null;
	if(!vazia_String(s)){
		if(!vazia_LString(aux)){
			auxS = (char*)malloc(TAM_PATH);
			strcpy(auxS,"\0");
			strcat(auxS,aux->Info);
			while(!vazia_LString(aux->prox)){
				aux = aux->prox;
	        	strcat(auxS,"\\");
	        	strcat(auxS,aux->Info);
			}
		}
	}	
	return auxS;
}

#endif
